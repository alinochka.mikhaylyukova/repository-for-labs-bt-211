import random
a = [[random.randrange(100) for i in range(2)] for j in range(10)]
print(a)

import csv
FILENAME = 'user.csv'
users = [a]
with open(FILENAME, "w", newline="") as file:
    writer = csv.writer(file)
    writer.writerows(users)
FILENAME = "user.csv"

with open(FILENAME, "r", newline="") as file:
    reader = csv.reader(file)
    for row in reader:
        print(row[0], " - ", row[1])
